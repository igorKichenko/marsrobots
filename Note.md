## What would I do different if I had more time:
* Split application into different modules. I've tried to show this, by packages structure.
* Added more test. I've added few tests, but do not have full coverage + lucking UI tests.
* Worked more on UI. Added some info to start screen so user could understand better how to use an application, plus navigation menu and UI in general looks ugly :)
* Found better way how to check Network availability. It is abstracted, but final implementation requires reference to context, which is not great.
* Added mechanism for caching images (so before fetching them from network, you could check when you did it last time, so you could do it only when necessary)
