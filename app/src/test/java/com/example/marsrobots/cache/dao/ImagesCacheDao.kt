package com.example.marsrobots.cache.dao

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import com.example.marsrobots.cache.db.ImagesDataBase
import com.example.marsrobots.test.TestFactory
import org.junit.After
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment


@RunWith(RobolectricTestRunner::class)

class ImagesCacheDao {

    @Rule
    @JvmField var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val database = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application.applicationContext,
            ImagesDataBase::class.java)
            .allowMainThreadQueries()
            .build()

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun getImagesReturnsData() {
        val cachedImage = TestFactory.createCachedImageModel()
        database.cachedImagesDao().saveImages(listOf(cachedImage))

        val testObserver = database.cachedImagesDao().getImages().test()
        testObserver.assertValue(listOf(cachedImage))
    }

    @Test
    fun clearCacheCleansDb() {
        val cachedImage = TestFactory.createCachedImageModel()
        database.cachedImagesDao().saveImages(listOf(cachedImage))

        database.cachedImagesDao().clearImages()

        val testObserver = database.cachedImagesDao().getImages().test()
        testObserver.assertValue(emptyList())
    }

}