package com.example.marsrobots.cache.mapper

import com.example.marsrobots.cache.model.CachedImageModel
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.test.TestFactory
import org.junit.Assert
import org.junit.Test


class CachedMapperTest {

    private val mapper: CacheMapperImpl = CacheMapperImpl()

    @Test
    fun testConvertFormCached() {
        val cached = TestFactory.createCachedImageModel()
        assertValues(mapper.mapFromCache(cached), cached)
    }

    @Test
    fun testConvertToCache() {
        val marsImage = TestFactory.createMarsImageItem()
        assertValues(marsImage, mapper.mapToCache(marsImage))
    }

    private fun assertValues(marsImage: MarsImage, cachedImage: CachedImageModel) {
        Assert.assertEquals(marsImage.id, cachedImage.id)
        Assert.assertEquals(marsImage.title, cachedImage.title)
        Assert.assertEquals(marsImage.dateCreated, cachedImage.dateCreated)
        Assert.assertEquals(marsImage.url, cachedImage.url)
    }
}