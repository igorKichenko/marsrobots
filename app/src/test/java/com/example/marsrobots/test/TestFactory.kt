package com.example.marsrobots.test

import com.example.marsrobots.cache.model.CachedImageModel
import com.example.marsrobots.domain.data.MarsImage
import java.util.*


/**
 * Created with Android Studio.
 * Author: Igor Kichenko
 * Email: ikichenko@luxoft.com
 * Company: Luxoft - Mobile development
 * Date: 2/24/19
 * Time: 15:09
 */

object TestFactory {

    fun createRandomString(): String {
        return UUID.randomUUID().toString()
    }

    fun createMarsImageItem(): MarsImage {
        return MarsImage(createRandomString(), createRandomString(), createRandomString(), createRandomString())
    }

    fun createListOfMarsImages(count: Int): List<MarsImage> {
        val result = mutableListOf<MarsImage>()
        repeat(count) {
            result.add(createMarsImageItem())
        }

        return result
    }

    fun createCachedImageModel(): CachedImageModel {
        return CachedImageModel(createRandomString(), createRandomString(), createRandomString(), createRandomString())
    }

    fun createListOfCachedImageModel(count: Int): List<CachedImageModel> {
        val result = mutableListOf<CachedImageModel>()
        repeat(count) {
            result.add(createCachedImageModel())
        }

        return result
    }
}