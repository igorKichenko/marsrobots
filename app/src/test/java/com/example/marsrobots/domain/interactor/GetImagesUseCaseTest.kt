package com.example.marsrobots.domain.interactor

import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.domain.executor.PostExecutionThread
import com.example.marsrobots.domain.repository.Repository
import com.example.marsrobots.test.TestFactory
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Test


class GetImagesUseCaseTest {

    val repository = mock<Repository>()
    val postExecutionThread = mock<PostExecutionThread>()

    val getImagesUseCase = GetImagesUseCase(repository, postExecutionThread)

    @Test
    fun getImagesCompletes() {
        stubRepositoryGetImages(Observable.just(TestFactory.createListOfMarsImages(2)))
        val testObserver = getImagesUseCase.buildUseCaseObservable().test()
        testObserver.assertComplete()
    }

    @Test
    fun getImagesReturnsData() {
        val images = TestFactory.createListOfMarsImages(2)
        stubRepositoryGetImages(Observable.just(images))
        val testObserver = getImagesUseCase.buildUseCaseObservable().test()
        testObserver.assertValue(images)
    }

    private fun stubRepositoryGetImages(observable: Observable<List<MarsImage>>) {
        whenever(repository.getImages()).thenReturn(observable)
    }
}