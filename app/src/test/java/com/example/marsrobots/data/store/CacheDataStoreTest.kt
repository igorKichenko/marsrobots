package com.example.marsrobots.data.store

import com.example.marsrobots.data.repository.ImagesCache
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.test.TestFactory
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Completable
import io.reactivex.Observable
import org.junit.Test


class CacheDataStoreTest {

    val cache = mock<ImagesCache>()
    val cacheDataStore = CacheDataStore(cache)


    @Test
    fun getImagesCompletes() {
        val images = TestFactory.createListOfMarsImages(3)
        stubGetImages(Observable.just(images))
        val testObserver = cacheDataStore.getImages().test()
        testObserver.assertComplete()
    }

    @Test
    fun getImagesReturnsData() {
        val images = TestFactory.createListOfMarsImages(3)
        stubGetImages(Observable.just(images))
        val testObserver = cacheDataStore.getImages().test()
        testObserver.assertValue(images)
    }

    @Test
    fun saveImagesCompletes() {
        val images = TestFactory.createListOfMarsImages(3)
        stubSaveImages(Completable.complete())
        val testObserver = cacheDataStore.saveImages(images).test()
        testObserver.assertComplete()
    }

    @Test
    fun saveImagesCacheSaveMethodCalled() {
        val images = TestFactory.createListOfMarsImages(3)
        stubSaveImages(Completable.complete())
        val testObserver = cacheDataStore.saveImages(images).test()
        verify(cache).saveImages(images)
    }

    private fun stubSaveImages(completable: Completable) {
        whenever(cache.saveImages(any())).thenReturn(completable)
    }

    private fun stubGetImages(observable: Observable<List<MarsImage>>) {
        whenever(cache.getImages()).thenReturn(observable)
    }
}