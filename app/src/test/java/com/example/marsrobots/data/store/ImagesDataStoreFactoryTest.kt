package com.example.marsrobots.data.store

import com.nhaarman.mockito_kotlin.mock
import org.junit.Assert
import org.junit.Test


class ImagesDataStoreFactoryTest {
    private val cacheDataStore = mock<CacheDataStore>()
    private val remoteDataStore = mock<RemoteDataStore>()


    private val factory = ImagesDataStoreFactory(cacheDataStore, remoteDataStore)

    @Test
    fun factoryReturnsCachedStoreWhenNoNetwork() {
        Assert.assertEquals(cacheDataStore, factory.getDataStore(false))
    }

    @Test
    fun factoryReturnsRemoteStoreWhenNetworkAwailable() {
        Assert.assertEquals(remoteDataStore, factory.getDataStore(true))
    }

    @Test
    fun factoryReturnsCachedStroe() {
        Assert.assertEquals(cacheDataStore, factory.getCachedDataStore())
    }
}