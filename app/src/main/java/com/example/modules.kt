package com.example

import com.example.marsrobots.BuildConfig
import com.example.marsrobots.cache.ImagesCacheImpl
import com.example.marsrobots.cache.db.ImagesDataBase
import com.example.marsrobots.cache.mapper.CacheMapper
import com.example.marsrobots.cache.mapper.CacheMapperImpl
import com.example.marsrobots.cache.model.CachedImageModel
import com.example.marsrobots.data.ImagesDataRepository
import com.example.marsrobots.data.repository.ImagesCache
import com.example.marsrobots.data.repository.ImagesRemote
import com.example.marsrobots.data.store.CacheDataStore
import com.example.marsrobots.data.store.ImagesDataStoreFactory
import com.example.marsrobots.data.store.NetworkAvailabilityProvider
import com.example.marsrobots.data.store.RemoteDataStore
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.domain.executor.PostExecutionThread
import com.example.marsrobots.domain.interactor.GetImagesUseCase
import com.example.marsrobots.domain.repository.Repository
import com.example.marsrobots.presentation.BrowseImagesViewModel
import com.example.marsrobots.remote.ImagesRemoteImpl
import com.example.marsrobots.remote.mapper.RemoteModelMapper
import com.example.marsrobots.remote.mapper.RemoteToDomainMapperImpl
import com.example.marsrobots.remote.model.RemoteMarsImageModel
import com.example.marsrobots.remote.service.NasaApiServiceFactory
import com.example.marsrobots.ui.AndroidUiThread
import com.example.marsrobots.ui.NetworkConnectionHelper
import com.example.marsrobots.ui.adapter.MarsImagesAdapter
import org.koin.android.ext.koin.androidApplication
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


val applicationModule = module {
    //cache
    single("database") { ImagesDataBase.getInstance(androidApplication()) }
    factory<CacheMapper<CachedImageModel, MarsImage>>("cacheMapper") { CacheMapperImpl() }
    factory<ImagesCache>("imagesCache") { ImagesCacheImpl(get("database"), get("cacheMapper")) }

    //remote
    factory("nasaApiService") { NasaApiServiceFactory.makeNasaApiServiceFactory(BuildConfig.DEBUG) }
    factory<RemoteModelMapper<RemoteMarsImageModel, MarsImage>>("remoteMapper") { RemoteToDomainMapperImpl() }
    factory<ImagesRemote>("imagesRemote") {ImagesRemoteImpl(get("nasaApiService"), get("remoteMapper"))}

    //data store
    factory<CacheDataStore>("cacheDataStore") { CacheDataStore(get("imagesCache"))}
    factory<RemoteDataStore>("remoteDataStore") { RemoteDataStore(get("imagesRemote"))}
    factory<ImagesDataStoreFactory>("dataStoreFactory") { ImagesDataStoreFactory(get("cacheDataStore"), get("remoteDataStore")) }

    //repository
    factory<NetworkAvailabilityProvider>("networkAvailibilityProvider") {NetworkConnectionHelper(androidApplication())}
    factory<Repository>("repository"){ImagesDataRepository(get("dataStoreFactory"), get("networkAvailibilityProvider"))}

    //iteractor
    single<PostExecutionThread>("postExecutionThread") {AndroidUiThread()}
    factory("getImagesUseCase") {GetImagesUseCase(get("repository"), get("postExecutionThread"))}

    //presentation
    viewModel("browseImagesViewModel") {BrowseImagesViewModel(get("getImagesUseCase"))}

    //ui
    factory("marsImagesAdapter") {MarsImagesAdapter()}

}

