package com.example.marsrobots.cache.db




object CachedImageConstants {
    const val TABLE_NAME = "images"
    const val QUERY_IMAGES = "SELECT * FROM $TABLE_NAME"
    const val DELETE_IMAGES = "DELETE FROM $TABLE_NAME"
}