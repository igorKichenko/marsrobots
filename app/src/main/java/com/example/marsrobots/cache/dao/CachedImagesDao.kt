package com.example.marsrobots.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.marsrobots.cache.db.CachedImageConstants
import com.example.marsrobots.cache.model.CachedImageModel
import io.reactivex.Flowable




@Dao
abstract class CachedImagesDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    @JvmSuppressWildcards
    abstract fun saveImages(images: List<CachedImageModel>)

    @Query(CachedImageConstants.QUERY_IMAGES)
    @JvmSuppressWildcards
    abstract fun getImages(): Flowable<List<CachedImageModel>>

    @Query(CachedImageConstants.DELETE_IMAGES)
    @JvmSuppressWildcards
    abstract fun clearImages()
}