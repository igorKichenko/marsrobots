package com.example.marsrobots.cache

import com.example.marsrobots.cache.db.ImagesDataBase
import com.example.marsrobots.cache.mapper.CacheMapper
import com.example.marsrobots.cache.model.CachedImageModel
import com.example.marsrobots.data.repository.ImagesCache
import com.example.marsrobots.domain.data.MarsImage
import io.reactivex.Completable
import io.reactivex.Observable




class ImagesCacheImpl(private val imagesDataBase: ImagesDataBase,
                      private val mapper: CacheMapper<CachedImageModel, MarsImage>)
    : ImagesCache {

    override fun getImages(): Observable<List<MarsImage>> {
        return imagesDataBase.cachedImagesDao().getImages()
                .toObservable()
                .map {
                    it.map {
                        mapper.mapFromCache(it)
                    }
                }
    }

    override fun saveImages(images: List<MarsImage>): Completable {
        return Completable.defer {
            imagesDataBase.cachedImagesDao()
                    .saveImages(images.map { mapper.mapToCache(it) })
            Completable.complete()
        }
    }

    override fun clearImges(): Completable {
        return Completable.defer {
            imagesDataBase.cachedImagesDao()
                    .clearImages()
            Completable.complete()
        }
    }
}