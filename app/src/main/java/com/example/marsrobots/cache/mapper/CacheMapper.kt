package com.example.marsrobots.cache.mapper




interface CacheMapper<C, D> {

    fun mapToCache(domainModel: D): C

    fun mapFromCache(cacheModel: C): D
}