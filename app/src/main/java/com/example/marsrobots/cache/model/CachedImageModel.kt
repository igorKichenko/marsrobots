package com.example.marsrobots.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.marsrobots.cache.db.CachedImageConstants


@Entity(tableName = CachedImageConstants.TABLE_NAME)
data class CachedImageModel(
        @PrimaryKey val id: String,
        val title: String,
        val dateCreated: String,
        val url: String)