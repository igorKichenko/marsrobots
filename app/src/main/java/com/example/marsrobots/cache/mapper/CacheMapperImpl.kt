package com.example.marsrobots.cache.mapper

import com.example.marsrobots.cache.model.CachedImageModel
import com.example.marsrobots.domain.data.MarsImage



class CacheMapperImpl : CacheMapper<CachedImageModel, MarsImage> {

    override fun mapToCache(domainModel: MarsImage): CachedImageModel {
        return CachedImageModel(domainModel.id, domainModel.title, domainModel.dateCreated, domainModel.url)
    }

    override fun mapFromCache(cacheModel: CachedImageModel): MarsImage {
        return MarsImage(cacheModel.id, cacheModel.title, cacheModel.dateCreated, cacheModel.url)
    }
}