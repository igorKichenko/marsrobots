package com.example.marsrobots.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.marsrobots.cache.dao.CachedImagesDao
import com.example.marsrobots.cache.model.CachedImageModel


@Database(entities = arrayOf(CachedImageModel::class), version = 1)
abstract class ImagesDataBase : RoomDatabase() {

    abstract fun cachedImagesDao(): CachedImagesDao

    companion object {
        private var INSTANCE: ImagesDataBase? = null
        private val lock = Any()

        fun getInstance(context: Context): ImagesDataBase {
            if (INSTANCE == null) {
                synchronized(lock) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                ImagesDataBase::class.java, "projects.db").build()
                    }
                }
                return INSTANCE as ImagesDataBase
            }
            return INSTANCE as ImagesDataBase
        }
    }
}