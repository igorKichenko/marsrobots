package com.example.marsrobots.presentation.state



enum class ResourceState {
    LOADING, SUCCESS, ERROR
}