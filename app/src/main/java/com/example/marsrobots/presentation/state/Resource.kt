package com.example.marsrobots.presentation.state


class Resource<T> (
        val status: ResourceState,
        val data: T? = null,
        val message: String? = null)