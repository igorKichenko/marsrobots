package com.example.marsrobots.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.domain.interactor.GetImagesUseCase
import com.example.marsrobots.presentation.state.Resource
import com.example.marsrobots.presentation.state.ResourceState
import io.reactivex.observers.DisposableObserver



class BrowseImagesViewModel (private val getImages: GetImagesUseCase) : ViewModel() {

    private val liveData: MutableLiveData<Resource<List<MarsImage>>> = MutableLiveData()

    fun getImagesLiveData(): LiveData<Resource<List<MarsImage>>> {
        return liveData
    }

    fun getImages() {
        liveData.postValue(Resource(status = ResourceState.LOADING))
        getImages.execute(ImagesObserver())
    }

    override fun onCleared() {
        getImages.dispose()
        super.onCleared()
    }

    private inner class ImagesObserver : DisposableObserver<List<MarsImage>>() {
        override fun onComplete() {

        }

        override fun onNext(t: List<MarsImage>) {
            liveData.postValue(Resource(status = ResourceState.SUCCESS, data = t))
        }

        override fun onError(e: Throwable) {
            liveData.postValue(Resource(status = ResourceState.ERROR, message = e.localizedMessage))
        }

    }
}