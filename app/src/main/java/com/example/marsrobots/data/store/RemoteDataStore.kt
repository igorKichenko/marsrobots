package com.example.marsrobots.data.store

import com.example.marsrobots.data.repository.ImagesDataStore
import com.example.marsrobots.data.repository.ImagesRemote
import com.example.marsrobots.domain.data.MarsImage
import io.reactivex.Completable
import io.reactivex.Observable
import java.lang.UnsupportedOperationException




open class RemoteDataStore(private val remote: ImagesRemote) : ImagesDataStore {

    override fun getImages(): Observable<List<MarsImage>> {
        return remote.getImages()
    }

    override fun saveImages(images: List<MarsImage>): Completable {
        throw UnsupportedOperationException ("Could not call save images in remote data store")
    }

    override fun clearImges(): Completable {
        throw UnsupportedOperationException ("Could not call clear images in remote data store")
    }
}