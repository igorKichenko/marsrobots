package com.example.marsrobots.data

import com.example.marsrobots.data.store.ImagesDataStoreFactory
import com.example.marsrobots.data.store.NetworkAvailabilityProvider
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.domain.repository.Repository
import io.reactivex.Observable



class ImagesDataRepository(private val factory: ImagesDataStoreFactory,
                           private val networkAvailabilityProvider: NetworkAvailabilityProvider)
    : Repository{
    override fun getImages(): Observable<List<MarsImage>> {
        return factory.getDataStore(networkAvailabilityProvider.isNetworkAvailable())
                .getImages()
                .flatMap { images ->
                    factory.getCachedDataStore().saveImages(images)
                            .andThen(Observable.just(images))
                }
    }

}