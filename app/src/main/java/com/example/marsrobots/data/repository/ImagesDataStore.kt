package com.example.marsrobots.data.repository

import com.example.marsrobots.domain.data.MarsImage
import io.reactivex.Completable
import io.reactivex.Observable




interface ImagesDataStore {

    fun getImages(): Observable<List<MarsImage>>

    fun saveImages(images: List<MarsImage>): Completable

    fun clearImges(): Completable
}