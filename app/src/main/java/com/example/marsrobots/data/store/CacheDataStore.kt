package com.example.marsrobots.data.store

import com.example.marsrobots.data.repository.ImagesCache
import com.example.marsrobots.data.repository.ImagesDataStore
import com.example.marsrobots.domain.data.MarsImage
import io.reactivex.Completable
import io.reactivex.Observable


open class CacheDataStore(private val cache: ImagesCache) : ImagesDataStore {

    override fun getImages(): Observable<List<MarsImage>> {
        return cache.getImages()
    }

    override fun saveImages(images: List<MarsImage>): Completable {
        return cache.saveImages(images)
    }

    override fun clearImges(): Completable {
        return cache.clearImges()
    }

}