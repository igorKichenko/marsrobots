package com.example.marsrobots.data.store


interface NetworkAvailabilityProvider {
    fun isNetworkAvailable(): Boolean
}