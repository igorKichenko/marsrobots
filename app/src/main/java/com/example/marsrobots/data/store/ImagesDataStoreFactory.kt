package com.example.marsrobots.data.store

import com.example.marsrobots.data.repository.ImagesDataStore


class ImagesDataStoreFactory (private val cacheDataStore: CacheDataStore,
                              private val remoteDataStore: RemoteDataStore) {

    open fun getDataStore(isNetworkAvailable: Boolean): ImagesDataStore {
       return if (isNetworkAvailable) remoteDataStore else cacheDataStore
    }

    open fun getCachedDataStore(): ImagesDataStore {
        return cacheDataStore
    }

}