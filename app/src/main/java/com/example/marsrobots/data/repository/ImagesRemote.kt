package com.example.marsrobots.data.repository

import com.example.marsrobots.domain.data.MarsImage
import io.reactivex.Observable




interface ImagesRemote {
    fun getImages(): Observable<List<MarsImage>>
}