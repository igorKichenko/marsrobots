package com.example.marsrobots.remote.mapper




interface RemoteModelMapper<in R, out D> {

    fun mapRemoteModelToDomainModel(remote: R): D

}