package com.example.marsrobots.remote.service

import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit




object NasaApiServiceFactory {

    fun makeNasaApiServiceFactory(isDebug: Boolean): NasaApiService {
        val okHttpClient = makeHttpOkClient(makeLoggingIntrceptor(isDebug))
        return makeNasaApiService(okHttpClient, Gson())
    }

    private fun makeNasaApiService(okHttpClient: OkHttpClient, gson: Gson): NasaApiService {
        val retrofit = Retrofit.Builder()
                .baseUrl("https://images-api.nasa.gov/")
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        return retrofit.create(NasaApiService::class.java)
    }

    private fun makeHttpOkClient(loggingInterceptor: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .connectTimeout(120, TimeUnit.SECONDS)
                .readTimeout(120, TimeUnit.SECONDS)
                .build()
    }


    private fun makeLoggingIntrceptor(isDebug: Boolean): HttpLoggingInterceptor {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = if (isDebug) {
            HttpLoggingInterceptor.Level.BODY
        } else {
            HttpLoggingInterceptor.Level.NONE
        }
        return loggingInterceptor
    }
}