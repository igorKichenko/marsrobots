package com.example.marsrobots.remote.mapper

import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.remote.model.RemoteMarsImageModel



class RemoteToDomainMapperImpl : RemoteModelMapper<RemoteMarsImageModel, MarsImage> {

    override fun mapRemoteModelToDomainModel(remote: RemoteMarsImageModel): MarsImage {
        return MarsImage(remote.data[0].id, remote.data[0].title, remote.data[0].date, remote.links[0].url)
    }
}