package com.example.marsrobots.remote.model

import com.google.gson.annotations.SerializedName




class RemoteMarsImageData (@SerializedName("title") val title: String,
                           @SerializedName("date_created") val date: String,
                           @SerializedName("nasa_id") val id: String)