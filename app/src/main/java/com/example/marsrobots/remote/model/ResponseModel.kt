package com.example.marsrobots.remote.model

import com.google.gson.annotations.SerializedName



class ResponseModel (@SerializedName("collection") val collection: MarsImagesCollection)