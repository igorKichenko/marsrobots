package com.example.marsrobots.remote

import com.example.marsrobots.data.repository.ImagesRemote
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.remote.mapper.RemoteModelMapper
import com.example.marsrobots.remote.model.RemoteMarsImageModel
import com.example.marsrobots.remote.service.NasaApiService
import io.reactivex.Observable



class ImagesRemoteImpl(private val service: NasaApiService,
                       private val mapper: RemoteModelMapper<RemoteMarsImageModel, MarsImage>)
    : ImagesRemote {
    override fun getImages(): Observable<List<MarsImage>> {
        return service.getImages()
                .map {
                    it.collection.items.map {
                        mapper.mapRemoteModelToDomainModel(it)
                    }
                }
    }


}