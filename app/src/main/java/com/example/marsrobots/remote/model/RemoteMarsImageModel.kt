package com.example.marsrobots.remote.model

import com.google.gson.annotations.SerializedName



class RemoteMarsImageModel (@SerializedName("data")val data: List<RemoteMarsImageData>,
                            @SerializedName("links") val links: List<RemoteMarsImageLinks>)