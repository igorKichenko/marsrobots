package com.example.marsrobots.remote.service

import com.example.marsrobots.remote.model.ResponseModel
import io.reactivex.Observable
import retrofit2.http.GET



interface NasaApiService {
    @GET("search?q=mars&media_type=image")
    fun getImages() : Observable<ResponseModel>
}