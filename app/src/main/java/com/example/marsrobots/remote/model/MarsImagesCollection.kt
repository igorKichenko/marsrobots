package com.example.marsrobots.remote.model

import com.google.gson.annotations.SerializedName


class MarsImagesCollection (@SerializedName("items")val items: List<RemoteMarsImageModel>)