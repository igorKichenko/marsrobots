package com.example.marsrobots.remote.model

import com.google.gson.annotations.SerializedName



class RemoteMarsImageLinks (@SerializedName("href") val url: String)