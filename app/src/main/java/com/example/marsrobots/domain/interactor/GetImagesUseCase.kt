package com.example.marsrobots.domain.interactor

import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.domain.executor.PostExecutionThread
import com.example.marsrobots.domain.repository.Repository
import io.reactivex.Observable



class GetImagesUseCase(val repository: Repository, val postExecutionThread: PostExecutionThread)
    : ObservableUseCase<List<MarsImage>, Nothing?>(postExecutionThread) {

    override fun buildUseCaseObservable(params: Nothing?): Observable<List<MarsImage>> {
        return repository.getImages()
    }
}