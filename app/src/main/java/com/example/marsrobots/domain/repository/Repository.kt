package com.example.marsrobots.domain.repository

import com.example.marsrobots.domain.data.MarsImage
import io.reactivex.Observable


interface Repository {
    fun getImages(): Observable<List<MarsImage>>
}