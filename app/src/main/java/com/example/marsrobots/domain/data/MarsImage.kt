package com.example.marsrobots.domain.data


class MarsImage(val id: String,
                val title: String,
                val dateCreated: String,
                val url: String)