package com.example.marsrobots

import android.app.Application
import com.example.applicationModule
import org.koin.android.ext.android.startKoin
import timber.log.Timber




class MarsApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(applicationModule))
        setUpTimber()
    }

    private fun setUpTimber() {
        Timber.plant(TimberCustomTree())
    }

    class TimberCustomTree : Timber.DebugTree() {
        override fun createStackElementTag(element: StackTraceElement): String? {
            return "${super.createStackElementTag(element)}: in method:${element.methodName} at line: ${element.lineNumber}"
        }
    }
}