package com.example.marsrobots.ui

import android.content.Context
import android.net.ConnectivityManager
import com.example.marsrobots.data.store.NetworkAvailabilityProvider


class NetworkConnectionHelper(private val context: Context) : NetworkAvailabilityProvider {
    override fun isNetworkAvailable(): Boolean {
        val connectivityManager: ConnectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return if (networkInfo != null) networkInfo.isConnected else false
    }
}