package com.example.marsrobots.ui


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager

import com.example.marsrobots.R
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.presentation.BrowseImagesViewModel
import com.example.marsrobots.presentation.state.Resource
import com.example.marsrobots.presentation.state.ResourceState
import com.example.marsrobots.ui.adapter.MarsImagesAdapter
import kotlinx.android.synthetic.main.fragment_mars_images.*
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel
import timber.log.Timber

/**
 * A simple [Fragment] subclass.
 * Use the [MarsImagesFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class MarsImagesFragment : Fragment() {

    val adapter: MarsImagesAdapter by inject()

    val browseImagesViewModel: BrowseImagesViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("called")
        browseImagesViewModel.getImagesLiveData().observe(this, Observer {
            it?.let {
                onDataChanged(it)
            }
        })
        browseImagesViewModel.getImages()
    }

    override fun onStart() {
        super.onStart()
        setupImagesRecycler()
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mars_images, container, false)

    }

    fun setupImagesRecycler() {
        recycler_images.layoutManager = GridLayoutManager(this.context, 2)
        recycler_images.adapter = adapter
    }

    private fun onDataChanged(resource: Resource<List<MarsImage>>) {
        when (resource.status) {
            ResourceState.LOADING -> {
                updateScreenOnLoading()
            }
            ResourceState.SUCCESS -> {
                updateScreenOnSucces(resource.data)
            }
            ResourceState.ERROR -> {
                Timber.d("ERROR")
                Timber.d(resource.message)
            }
        }
    }

    private fun updateScreenOnLoading() {
        progress.visibility = View.VISIBLE
        recycler_images.visibility = View.GONE
    }

    private fun updateScreenOnSucces(images: List<MarsImage>?) {
        progress.visibility = View.GONE
        if (images != null) {
            adapter.marsImages = images
            adapter.notifyDataSetChanged()
            recycler_images.visibility = View.VISIBLE
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment MarsImagesFragment.
         */
        @JvmStatic
        fun newInstance() =
                MarsImagesFragment()
    }
}
