package com.example.marsrobots.ui.utils

import android.content.Context
import android.net.Uri
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.example.marsrobots.R


class UrlUtils {

    companion object {
        fun launchUrl(context: Context, url: String) {
            launchUrl(context, Uri.parse(url))
        }

        private fun launchUrl(context: Context, uri: Uri) {
            val customTabIntent = CustomTabsIntent.Builder()
                    .addDefaultShareMenuItem()
                    .setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
                    .setShowTitle(true)
                    .build()
            customTabIntent.launchUrl(context, uri)
        }
    }


}