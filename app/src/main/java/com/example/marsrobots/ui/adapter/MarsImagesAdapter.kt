package com.example.marsrobots.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.marsrobots.R
import com.example.marsrobots.domain.data.MarsImage
import com.example.marsrobots.ui.utils.UrlUtils


class MarsImagesAdapter : RecyclerView.Adapter<MarsImagesAdapter.ViewHolder>() {

    var marsImages: List<MarsImage> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val itemView = LayoutInflater
               .from(parent.context)
               .inflate(R.layout.image_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return marsImages.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val marsImage = marsImages[position]
        holder.titleText.text = marsImage.title
        holder.dateText.text = marsImage.dateCreated

        Glide.with(holder.itemView.context)
                .load(marsImage.url)
                .into(holder.marsImage)

        holder.cardView.setOnClickListener {
            UrlUtils.launchUrl(it.context, marsImage.url)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var marsImage: ImageView
        val titleText: TextView
        val dateText: TextView
        val cardView: CardView

        init {
            marsImage = view.findViewById(R.id.image_mars_image)
            titleText = view.findViewById(R.id.text_image_title)
            dateText = view.findViewById(R.id.text_image_date)
            cardView = view.findViewById(R.id.card_view_mars_item)
        }
    }
}