package com.example.marsrobots

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.example.marsrobots.ui.MarsImagesFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.layout_toolbar.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupActionBar()
        setupNavigationView()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            android.R.id.home -> {
                drawer_layout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp)
        }
    }

    private fun setupNavigationView() {
        nav_view.setNavigationItemSelectedListener { selectedItem ->
            selectedItem.isChecked = true
            drawer_layout.closeDrawers()

            when (selectedItem.itemId) {
                R.id.nav_to_images -> showImagesFragment()
            }

            true
        }
    }


    private fun showImagesFragment() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.content_frame)
        if (!(currentFragment is MarsImagesFragment)) {
            val transaction = supportFragmentManager.beginTransaction()
            transaction.replace(R.id.content_frame, MarsImagesFragment.newInstance())
            transaction.commit()
        }
    }
}
